const notifier = require('./');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.should();
const { expect } = chai;

describe('Notifier', () => {
    beforeEach(() => {
        notifier.reset();
    });

    it('should successfully init notifier if list of chats match', () => {
        notifier.init([
            { name: 'hipchat' },
            { name: 'rchat', options: {} },
            { name: 'email', options: {} }
        ]);
    });

    it('should fail init notifier if list of chats does not match', () => {
        expect(() => {
            notifier.init([
                { name: 'nonechat', options: {} }
            ])
        }).to.throw('Notifier does not exist');
    });

    it('should fail init if list is empty', () => {
        expect(notifier.init.bind(notifier)).to.throw('Notifier list must contain at least one item');
    });

    it('should fail send to not existing chat', () => {
       notifier.send('nonechat', {}).should.be.rejectedWith('Message for nonechat not sent: Cannot read property \'send\' of undefined');
    });
});
