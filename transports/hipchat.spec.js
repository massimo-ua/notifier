const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const should = chai.should();
const { expect } = chai;
const { hipchat } = require('./');

const data = {
    uri: 'http://test-hipchat.co',
    message: 'test message'
};

describe('Hipchat', () => {
    it('should successfully init', () => {
        const request = {
            post: async () => { resolve(true); }
        };
        const fakeHipchat = hipchat(request);
        expect(fakeHipchat.init()).to.be.true;
    });

    it('should successfully send', () => {
        const request = {
            post: async () => true
        };
        const fakeHipchat = hipchat(request);
        fakeHipchat.init();
        fakeHipchat.send(data).should.be.fulfilled;
    });

    it('should fail send', () => {
        const request = {
            post: async () => { reject(false); }
        };
        const fakeHipchat = hipchat(request);
        fakeHipchat.init();
        fakeHipchat.send(data).should.be.rejected;
    });
});
