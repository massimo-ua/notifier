const hipchat = require('./hipchat');
const rchat = require('./rchat');
const email = require('./email');

module.exports = {
    hipchat,
    rchat,
    email,
};
