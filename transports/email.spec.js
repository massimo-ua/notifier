const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const should = chai.should();
const { expect } = chai;
const { email } = require('./');

const nodemailer = {
    createTransport: () => {
        return { sendMail: async () => true };
    }
};
const smtpTransport = () => {
    return true;
};
const transport = {
    service: 'gmail',
    auth: {
        user: 'test.test@gmail.com',
        pass: 'test'
    }
};
const data = {
    from: 'test',
    to: 'tester',
    subject: 'Test subject',
    text: 'Test text'
};

describe('Email', () => {
    it('should successfully init', () => {
        const fakeEmail = email(nodemailer, smtpTransport);
        expect(fakeEmail.init(transport)).to.be.true;
    });

    it('should successfully send', () => {
        const fakeEmail = email(nodemailer, smtpTransport);
        fakeEmail.init(transport);
        fakeEmail.send(data).should.be.fulfilled;
    });

    it('should fail send', () => {
        const rejectNodemailer = {
            createTransport: () => {
                return { sendMail: async () => reject(false) };
            }
        };
        const fakeEmail = email(rejectNodemailer, smtpTransport);
        fakeEmail.init(transport);
        fakeEmail.send(data).should.be.rejected;
    });
});
