'use strict';
const requestInstance = require ('request-promise-native');

function hipchat (request = requestInstance) {
  return {
    init: function () {
      return true;
    },
    send: async function (data) {
      const options = {
        method: 'POST',
        uri: data.uri,
        body: {
          color: data.color || 'red',
          message: data.message,
          notify: data.notify || true,
          message_format: data.message_format || 'text',
        },
        json: true,
      };
      try {
        await request.post (options);
        return true;
      } catch (error) {
        throw error;
      }
    },
  };
}

module.exports = hipchat;
