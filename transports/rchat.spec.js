const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const should = chai.should();
const { expect } = chai;
const { rchat } = require('./');

const creds = {
    uri: 'http://test-rchat.co',
    username: 'test',
    password: 'test'
};

describe('Rchat', () => {
    it('should successfully init', () => {
        const request = {
            post: async () => { resolve(true); }
        };
        const fakeRchat = rchat(request);
        expect(fakeRchat.init(creds)).to.be.true;
    });

    it('should successfully send', () => {
        const request = {
            counter: 0,
            post: async () => {
                this.counter++;
                return (this.counter === 1) ? false : { data: { authToken: 't', userId: 1 }};
            }
        };
        const fakeRchat = rchat(request);
        fakeRchat.init(creds);
        fakeRchat.send({ data: 'data' }).should.be.fulfilled;
    });

    it('should fail send', () => {
        const request = {
            post: async () => { reject(false); }
        };
        const fakeRchat = rchat(request);
        fakeRchat.init(creds);
        fakeRchat.send({ data: 'data' }).should.be.rejected;
    });
});
