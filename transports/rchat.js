'use strict';
const requestInstance = require ('request-promise-native');
const TOKEN_TTL = 60000;

function rchat (request = requestInstance) {
  let uri, username, password, authToken, userId;
  let updated_at = 0;
  const setCreds = creds => {
    uri = creds.uri;
    username = creds.username;
    password = creds.password;
  };
  const authorize = async () => {
    const response = await connectRchat ({uri, username, password});
    authToken = response.authToken;
    userId = response.userId;
    updated_at = Date.now ();
  };
  const connectRchat = async creds => {
    const options = {
      method: 'POST',
      uri: `${creds.uri}/api/v1/login`,
      body: {
        username: creds.username,
        password: creds.password,
      },
      json: true,
    };
    const response = await request.post (options);
    return response.data;
  };
  const postMessage = async options => {
    await request.post (options);
    return true;
  };
  const isAuthorized = () => {
    return authToken && userId && Date.now () - updated_at < TOKEN_TTL;
  };
  return {
    init: function (credentials) {
      setCreds (credentials);
      return true;
    },
    send: async function (data) {
      if (isAuthorized()) {
        const options = {
          method: 'POST',
          uri: `${uri}/api/v1/chat.postMessage`,
          headers: {
            'X-Auth-Token': authToken,
            'X-User-Id': userId,
          },
          body: data,
          json: true,
        };
        return postMessage (options);
      }
      await authorize ();
      return this.send (data);
    },
  };
}

module.exports = rchat;
